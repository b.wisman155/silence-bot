extern crate teleborg;
#[macro_use]
extern crate lazy_static;

use teleborg::objects::Update;
use teleborg::{Dispatcher, Bot, Updater};

use std::sync::Mutex;

lazy_static!{
    static ref NAMES: Mutex<Vec<String>> = Mutex::new(Vec::new());
}

fn main() {
    let bot_token = "358481040:AAEh8j1zd7PvmpEiaylrGGWstMbfpoPsqOU".to_string();
    let mut dispatcher = Dispatcher::new();
    dispatcher.add_command_handler("silence", silence, true);
    dispatcher.add_command_handler("unsilence", un_silence, true);
    dispatcher.add_message_handler(message_handler);
    Updater::start(Some(bot_token), None, None, None, dispatcher);
}

fn silence(bot: &Bot, update: Update, args: Option<Vec<&str>>) {
    let message = update.clone().message.unwrap();
    let chat_id = update.message.unwrap().chat.id;
    let username = message.clone().from.unwrap().username.unwrap();
    let exists = NAMES.lock().unwrap().iter().position(|x| *x == username).is_some();
    if exists {
        bot.delete_message(&chat_id, &message.message_id);
        return;
    }
    if let Some(args) = args {
        if args.len() > 0 {
            let name = args[0];
            let mut names = NAMES.lock().unwrap();
            names.push(name.to_string());
            bot.send_message(&chat_id, name, None, None, None, None, None).ok();
        }
    }
}

fn un_silence(bot: &Bot, update: Update, args: Option<Vec<&str>>) {
    let chat_id = update.clone().message.unwrap().chat.id;
    let message = update.clone().message.unwrap();
    let chat_id = update.message.unwrap().chat.id;
    let username = message.clone().from.unwrap().username.unwrap();
    let exists = NAMES.lock().unwrap().iter().position(|x| *x == username).is_some();
    if exists {
        bot.delete_message(&chat_id, &message.message_id);
        return;
    }
    if let Some(args) = args {
        if args.len() > 0 {
            let name = args[0];
            let mut names = NAMES.lock().unwrap();
            let index = names.iter().position(|x| *x == name.to_string()).unwrap();
            names.remove(index);
            bot.send_message(&chat_id, name, None, None, None, None, None).ok();
        }
    }
}

fn message_handler(bot: &Bot, update: Update, _: Option<Vec<&str>>) {
    let message = update.clone().message.unwrap();
    let username = message.clone().from.unwrap().username.unwrap();
    let chat_id = update.message.unwrap().chat.id;
    let exists = NAMES.lock().unwrap().iter().position(|x| *x == username).is_some();
    if exists {
        bot.delete_message(&chat_id, &message.message_id);
    }
}
